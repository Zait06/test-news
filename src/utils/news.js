export const news = [
  {
    id: 65498,
    title: 'Mi primer nota',
    author: 'Jason Todd',
    date: '2022-01-06 15:39',
    text: 'Laboris aliqua veniam aute consequat aliquip in adipisicing.\n\nReprehenderit dolore consequat irure irure deserunt ad non nulla irure mollit consectetur anim dolor.\nUllamco esse deserunt cupidatat sit nisi ex mollit ut ipsum velit.',
    img: 'assets/imgs/jason.jpg',
  },
  {
    id: 56459,
    title: 'Esto no es un simulacro',
    author: 'Bruce Wane',
    date: '2022-03-01 20:50',
    text: 'Laboris aliqua veniam aute consequat aliquip in adipisicing. Reprehenderit dolore consequat irure irure deserunt ad non nulla irure mollit consectetur anim dolor. Ullamco esse deserunt cupidatat sit nisi ex mollit ut ipsum velit.',
    img: 'assets/imgs/bruce.jpeg',
  },
  {
    id: 98756,
    title: 'Como no sentirte inutil',
    author: 'Barbara Gordon',
    date: '2022-06-23 09:13',
    text: 'Laboris aliqua veniam aute consequat aliquip in adipisicing. Reprehenderit dolore consequat irure irure deserunt ad non nulla irure mollit consectetur anim dolor. Ullamco esse deserunt cupidatat sit nisi ex mollit ut ipsum velit.',
    img: 'assets/imgs/barbara.jpg',
  },
];
